const vn = {
  id: 99,

  releaseDate: '2013-02-08',

  title: 'Kara no Shoujo - The Second Episode',
  originalTitle: '虚ノ少女',
  aliases: ['Kara no Shoujo 2', '殻2', 'ウロ', 'Uro no Shoujo', 'KnS2'],
  descriptionHtml: `<p>
  In the period known as "pre-war," when Japan was taking its first
  steps into conflict... In the snowy mountains of the west, there is a
  settlement called Hitogata. In this place, with its strange custom of
  worshipping a clay doll called Hinna-sama, a woman is killed on the
  night of a festival.
</p>
<p>
  The villagers unanimously refer to this incident as "Hinna-sama's
  Curse." They say that she was cursed for worshipping Hinna-sama
  despite being unworthy.
</p>
<p>December, 1957.</p>
<p>
  <span class="spoiler">
    It's been almost two years since Kuchiki Touko was abducted from her
    hospital room.
  </span>
  Tokisaka Reiji's sister, Yukari, saves a man attempting to commit
  suicide. The man is a victim of "Hinna-sama's Curse," which has
  resurfaced in the present...
</p>
<p>
  Tokisaka Reiji acts to sever the delusions that span the war. All the
  while carrying his own delusions about Touko in his heart... At the
  same time, a religious group that was thought to have disbanded six
  years ago begins to move again.
</p>
<p>What could it be scheming?</p>
<p>
  Even if Heaven brings down a curse, it is human hands that enact it.
</p>`,

  coverImage: '/content/images/gung_0005pl.jpg',

  tags: [
    {
      id: 1471,
      type: 'tech',
      spoil: 0,
      frac: 1,
      name: 'Blind Choices',
    },
    {
      id: 459,
      type: 'tech',
      spoil: 0,
      frac: 1,
      name: 'Unlockable Routes',
    },
    {
      id: 19,
      type: 'cont',
      spoil: 0,
      frac: 1,
      name: 'Mystery',
    },
    {
      id: 553,
      type: 'tech',
      spoil: 0,
      frac: 1,
      name: 'Multiple Route Mystery',
    },
    {
      id: 981,
      type: 'tech',
      spoil: 0,
      frac: 1,
      name: 'Passage of Time',
    },
    {
      id: 323,
      type: 'cont',
      spoil: 0,
      frac: 0.93,
      name: 'Detective Work',
    },
    {
      id: 218,
      type: 'cont',
      spoil: 1,
      frac: 0.93,
      name: 'Traditionalist Family',
    },
    {
      id: 1646,
      type: 'tech',
      spoil: 2,
      frac: 0.93,
      name: 'Varied Title Screens',
    },
    {
      id: 153,
      type: 'cont',
      spoil: 0,
      frac: 0.9,
      name: 'Murder Mystery',
    },
    {
      id: 181,
      type: 'tech',
      spoil: 0,
      frac: 0.9,
      name: 'One True End',
    },
    {
      id: 1229,
      type: 'cont',
      spoil: 0,
      frac: 0.9,
      name: 'Heroine with Kimono',
    },
    {
      id: 676,
      type: 'cont',
      spoil: 0,
      frac: 0.9,
      name: 'Flashback',
    },
    {
      id: 992,
      type: 'ero',
      spoil: 2,
      frac: 0.9,
      name: 'Inbreeding',
    },
    {
      id: 848,
      type: 'cont',
      spoil: 2,
      frac: 0.9,
      name: 'Death of Heroine',
    },
    {
      id: 373,
      type: 'cont',
      spoil: 0,
      frac: 0.9,
      name: 'Student Heroine',
    },
    {
      id: 497,
      type: 'cont',
      spoil: 2,
      frac: 0.9,
      name: 'Heroine with Psychological Problems',
    },
    {
      id: 1059,
      type: 'tech',
      spoil: 0,
      frac: 0.87,
      name: 'Open Ending(s)',
    },
    {
      id: 757,
      type: 'cont',
      spoil: 0,
      frac: 0.87,
      name: 'Winter',
    },
    {
      id: 591,
      type: 'cont',
      spoil: 0,
      frac: 0.87,
      name: 'Private Detective Protagonist',
    },
    {
      id: 322,
      type: 'cont',
      spoil: 0,
      frac: 0.87,
      name: 'Crime',
    },
    {
      id: 672,
      type: 'tech',
      spoil: 2,
      frac: 0.87,
      name: 'Bad Ending(s)',
    },
    {
      id: 1474,
      type: 'tech',
      spoil: 0,
      frac: 0.87,
      name: 'Ending List',
    },
    {
      id: 1474,
      type: 'ero',
      spoil: 0,
      frac: 0.87,
      name: 'Test ero',
    },
  ],

  releaseData: [
    {
      lang: 'ja',
      name: 'Japanese',
      releases: [
        {
          id: 24086,
          date: '2012-11-21',
          name: 'Kara no Shoujo - Trial Edition',
          platforms: ['win'],
          url:
            'http://www.gungnir.co.jp/innocentgrey/products/pro_vacant/vacant_special_trial.html',
          developer: { id: 277, name: 'Innocent Grey' },
          publisher: { id: 277, name: 'Innocent Grey' },
        },
        {
          id: 11580,
          date: '2013-02-08',
          name: 'Kara no Shoujo - First Press Limited Edition',
          platforms: ['win'],
          url: 'http://www.gungnir.co.jp/innocentgrey/products/pro_vacant/',
          developer: { id: 277, name: 'Innocent Grey' },
          publisher: { id: 277, name: 'Innocent Grey' },
        },
      ],
    },
    {
      lang: 'en',
      name: 'English',
      releases: [
        {
          id: 41657,
          date: '2013-02-08',
          name: 'Kara no Shojo - The Second Episode - Trial Edition',
          platforms: ['web', 'lin', 'mac', 'win'],
          url: 'http://mangagamer.org/kns2/',
          developer: { id: 277, name: 'Innocent Grey' },
          publisher: { id: 428, name: 'MangaGamer' },
        },
      ],
    },
  ],

  creditGroups: [
    {
      name: 'Scenario',
      credits: [
        { staff: { id: 21, name: 'Suzuka Miya' } },
        {
          staff: { id: 3588, name: 'Towazuki Shingo' },
          note: 'Assistant writer',
        },
      ],
    },
    {
      name: 'Artist',
      credits: [{ staff: { id: 19, name: 'Sugina Miki' } }],
    },
    {
      name: 'Composeer ',
      credits: [{ staff: { id: 39, name: 'MANYO' } }],
    },
    {
      name: 'Director',
      credits: [{ staff: { id: 19, name: 'Sugina Miki' } }],
    },
    {
      name: 'Vocals',
      credits: [
        {
          staff: { id: 22, name: 'Shimotsuki Haruka' },
          note: 'Main Theme "Tsuki no Uro", ED "Solenoid"',
        },
        {
          staff: { id: 941, name: 'Suzuyu' },
          note: 'Second OP "Hisui no Miu"',
        },
      ],
    },
    {
      name: 'Staff',
      credits: [
        {
          staff: { id: 2829, name: 'Mizunoto Itsuya' },
          note: 'Movie',
        },
        {
          staff: { id: 11878, name: 'Toyo' },
          note: 'Graphics supervision',
        },
      ],
    },
  ],

  voteStats: [
    { score: 10, count: 293 },
    { score: 9, count: 601 },
    { score: 8, count: 402 },
    { score: 7, count: 165 },
    { score: 6, count: 52 },
    { score: 5, count: 21 },
    { score: 4, count: 13 },
    { score: 3, count: 6 },
    { score: 2, count: 4 },
    { score: 1, count: 2 },
  ],

  voteScore: 8.28,
  lengthFormatted: '30 - 50 hours',

  recentVotes: [
    {
      user: { id: 143493, name: 'nero-shuberg' },
      score: 8,
      date: '2018-02-14',
    },
    { user: { id: 132257, name: 'ultima21' }, score: 9, date: '2018-02-14' },
    { user: { id: 17448, name: 'mafuyu-chan' }, score: 9, date: '2018-02-12' },
    { user: { id: 143349, name: 'izanathosiv' }, score: 8, date: '2018-02-11' },
    { user: { id: 143314, name: 'tsukasawm' }, score: 7, date: '2018-02-10' },
    { user: { id: 143143, name: 'dread' }, score: 1, date: '2018-02-07' },
    { user: { id: 131387, name: 'higuchiken' }, score: 9, date: '2018-02-06' },
    { user: { id: 124761, name: 'azoth' }, score: 8, date: '2018-02-05' },
  ],

  gallery: [
    {
      release: {
        name: 'Kara no Shoujo - First Press Limited Edition',
        lang: 'jp',
      },
      images: [
        {
          id: 100,
          url: '../content/images/screenshots/42322.jpg',
          thumb: '../content/images/screenshots/42322-thumb.jpg',
          size: [1280, 720],
        },
        {
          id: 101,
          url: '../content/images/screenshots/47653.jpg',
          thumb: '../content/images/screenshots/47653-thumb.jpg',
          size: [1280, 716],
        },
        {
          id: 102,
          url: '../content/images/screenshots/47654.jpg',
          thumb: '../content/images/screenshots/47654-thumb.jpg',
          size: [1280, 715],
        },
        {
          id: 103,
          url: '../content/images/screenshots/42322.jpg',
          thumb: '../content/images/screenshots/42322-thumb.jpg',
          size: [1280, 720],
        },
        {
          id: 104,
          url: '../content/images/screenshots/47653.jpg',
          thumb: '../content/images/screenshots/47653-thumb.jpg',
          size: [1280, 716],
        },
        {
          id: 105,
          url: '../content/images/screenshots/47654.jpg',
          thumb: '../content/images/screenshots/47654-thumb.jpg',
          size: [1280, 715],
        },
      ],
    },
    {
      release: {
        name: 'Kara no Shoujo - The Second Episode',
        lang: 'en',
      },
      images: [
        {
          id: 106,
          url: '../content/images/screenshots/42322.jpg',
          thumb: '../content/images/screenshots/42322-thumb.jpg',
          size: [1280, 720],
        },
        {
          id: 107,
          url: '../content/images/screenshots/47653.jpg',
          thumb: '../content/images/screenshots/47653-thumb.jpg',
          size: [1280, 716],
        },
        {
          id: 108,
          url: '../content/images/screenshots/47654.jpg',
          thumb: '../content/images/screenshots/47654-thumb.jpg',
          size: [1280, 715],
        },
      ],
    },
  ],

  characters: [
    {
      id: 1,
      category: 'protagonist',
      name: 'Tokisaka Reiji',
      originalName: '時坂 玲人',
      gender: 'M',
      height: 178,
      weight: 65,
      bloodType: 'O',
      image: 'https://s2.vndb.org/ch/41/28541.jpg',
      birthday: { month: 12, day: 3 },
      age: 30,

      descriptionHtml: `Occupation: Private investigator<br>
  Hobbies: Reading (history books in particular)<br>
  <br>
  Our protagonist; a private detective that specializes in particularly brutal murder cases. He was formerly a detective in the police force but quit after he lost his fiance. Lives together with his little sister Yukari. He doesn’t stay at home much because he puts his work above everything else. Loves to pore over history books whenever he has free time. Reiji infiltrates Ouba Girls Academy as a substitute history teacher.<br>
  <br>
  ”It’s easy to understand why these criminals do what they do if you give it a little thought.”<br>
  <br>
  [From <a href="http://www.mangagamer.com/detail.php?goods_type=1&amp;product_code=79" rel="nofollow">MangaGamer</a>]`,

      vns: [
        { id: 810, name: 'Kara no Shoujo', role: { name: 'Protagonist' } },
        {
          id: 5922,
          name: 'Kara no Shoujo - The Second Episode',
          role: { name: 'Protagonist' },
        },
        {
          id: 17 - 12,
          name: 'Kara no Shoujo - The Last Episode',
          role: { name: 'Protagonist' },
        },
      ],

      voicedBy: [
        { staff: { id: 2548, name: 'Higeuchi Waruta' } },
        {
          staff: { id: 2548, name: 'Higeuchi Waruta' },
          note: '(FULL VOICE HD SIZE EDITION)',
        },
        {
          staff: { id: 2548, name: 'Higeuchi Waruta' },
          note: '(NEW CAST REMASTER EDITION)',
        },
        { staff: { id: 66, name: 'Takahashi Ganaranai' } },
      ],

      traitCategories: [
        {
          name: 'Hair',
          id: 1,
          tags: [
            { id: 11, name: 'Brown' },
            { id: 12, name: 'Curtained' },
            { id: 13, name: 'Parted in Middle' },
            { id: 14, name: 'Pubic Hair' },
            { id: 15, name: 'Short' },
            { id: 16, name: 'Wavy' },
          ],
        },
        {
          name: 'Eyes',
          id: 35,
          tags: [
            { id: 351, name: 'Black' },
            { id: 352, name: 'Hosome' },
          ],
        },
        {
          name: 'Body',
          id: 36,
          tags: [
            { id: 361, name: 'Average Height' },
            { id: 362, name: 'Pale' },
            { id: 363, name: 'Slim' },
            { id: 364, name: 'Young-adult' },
          ],
        },
        {
          name: 'Clothes',
          id: 37,
          tags: [
            { id: 371, name: 'Kimono' },
            { id: 372, name: 'Necktie' },
            { id: 373, name: 'Suit' },
          ],
        },
        {
          name: 'Items',
          id: 38,
          tags: [{ id: 381, name: 'Minor Spoiler', spoil: 1 }],
        },
        {
          name: 'Personality',
          id: 39,
          tags: [
            { id: 391, name: 'Brave' },
            { id: 392, name: 'Docile' },
            { id: 393, name: 'Kind' },
            { id: 394, name: 'Ore' },
            { id: 395, name: 'Proactive' },
            { id: 396, name: 'Smart' },
          ],
        },
        {
          name: 'Role',
          id: 40,
          tags: [
            { id: 4001, name: 'Coworker' },
            { id: 4002, name: 'Detective' },
            { id: 4003, name: 'Major Spoiler', spoil: 2 },
            { id: 4004, name: 'Friend' },
            { id: 4005, name: 'Full Brother' },
            { id: 4006, name: 'Not a Virgin' },
            { id: 4008, name: 'Older Brother' },
            { id: 4009, name: 'Secret Identity' },
            { id: 4010, name: 'Teacher' },
            { id: 4011, name: 'Minor Spoiler', spoil: 1 },
          ],
        },
        {
          name: 'Engages in',
          id: 41,
          tags: [
            { id: 411, name: 'Drinking' },
            { id: 412, name: 'Infiltration' },
            { id: 413, name: 'Investigation' },
            { id: 414, name: 'Planning' },
            { id: 415, name: 'Reading' },
            { id: 416, name: 'Minor Spoiler', spoil: 1 },
            { id: 417, name: 'Smoking' },
            { id: 418, name: 'Teasing' },
          ],
        },
        {
          name: 'Subject of',
          id: 42,
          tags: [
            { id: 421, name: 'Major Spoiler', spoil: 2 },
            { id: 422, name: 'Minor Spoiler', spoil: 1 },
            { id: 423, name: 'Major Spoiler', spoil: 2 },
            { id: 424, name: 'Grief' },
            { id: 425, name: 'Major Spoiler', spoil: 2 },
            { id: 426, name: 'Major Spoiler', spoil: 2 },
          ],
        },
        {
          name: 'Engages in (Sexual)',
          id: 43,
          tags: [
            { id: 4301, name: 'Anal Sex', sexual: true },
            { id: 4302, name: 'Minor Spoiler', sexual: true, spoil: 1 },
            { id: 4303, name: 'Cowgirl', sexual: true },
            { id: 4304, name: 'Doggy Style', sexual: true },
            { id: 4305, name: 'Intercrural Sex', sexual: true },
            { id: 4306, name: 'Missionary', sexual: true },
            { id: 4307, name: 'Nipple Sucking', sexual: true },
            { id: 4308, name: 'Nipple Teasing', sexual: true },
            { id: 4309, name: 'Outdoor Sex', sexual: true },
            { id: 4310, name: 'Quickie Fix', sexual: true },
            { id: 4311, name: 'Seventh Posture', sexual: true },
            { id: 4312, name: 'Sex in School Infirmary', sexual: true },
            { id: 4313, name: 'Sex in Water', sexual: true },
            { id: 4314, name: 'Major Spoiler', sexual: true, spoil: 2 },
            { id: 4315, name: 'Sitting Sex', sexual: true },
            { id: 4316, name: 'Sixty-nine', sexual: true },
            { id: 4317, name: 'Vaginal Fingering', sexual: true },
          ],
        },
        {
          name: 'Engages in (Sexual)',
          id: 1625,
          tags: [
            { id: 16251, name: 'Blowjob', sexual: true },
            { id: 16251, name: 'Boobjob', sexual: true },
            { id: 16251, name: 'Footjob', sexual: true },
          ],
        },
      ],
    },
  ],

  shops: [
    {
      name: 'MangaGamer',
      price: 'US$ 39.95',
      link:
        'https://www.mangagamer.com/r18/detail.php?product_code=139&af=3a123f6214695bfacaa881bd3117c693',
    },
  ],

  relations: [
    {
      typeName: 'Shares characters',
      vns: [
        { id: 515, title: 'Cartagra ~Tsuki kurui no Yamai~' },
        { id: 564, title: 'PP -Pianissimo- Ayatsuri Ningyou no Rinbu' },
      ],
    },
  ],
};

let lastId = 1;

const addChar = (name, category) => {
  lastId++;
  vn.characters.push({
    ...vn.characters[0],
    id: lastId,
    name,
    category,
    descriptionHtml: name,
  });
};

addChar('Masaki Tomoyuki', 'protagonist');

addChar('Hazuki Kyouko', 'main');
addChar('Kayahara Fuyumi', 'main');
addChar('Kayahara Yukiko', 'main');
addChar('Satsuki', 'main');
addChar('Sawashiro Nanako', 'main');
addChar('Shirosaki Michiru', 'main');
addChar('Tokisaka Yukari', 'main');
addChar('Torii Kohane', 'main');
addChar('Yaginuma Ryouichi', 'main');

addChar('Futami Yuu', 'side');
addChar('Hinagami Ayato', 'side');
addChar('Hinagami Hideomi', 'side');
addChar('Hinagami Karen', 'side');
addChar('Hinagami Mariko', 'side');
addChar('Hinagami Shizuru', 'side');
addChar('Kirimura Yuka', 'side');
addChar('Kuchiki Chizuru', 'side');
addChar('Kuchiki Chizuru', 'side');
addChar('Kuchiki Fumiya', 'side');
addChar('Kuchiki Fumiya', 'side');
addChar('Kuroya Naori', 'side');
addChar('Kuroya Sou', 'side');
addChar('Kuroya Yuzuru', 'side');
addChar('Saitou Tamaki', 'side');
addChar('Satou Ayumu', 'side');
addChar('Sawashiro Nanako', 'side');
addChar('Shigusa Kensei', 'side');
addChar('Shigusa Miya', 'side');
addChar('Shigusa Rioko', 'side');
addChar('Shigusa Saya', 'side');
addChar('Shigusa Yayoi', 'side');
addChar('Shigusa Yoshimitsu', 'side');
addChar('Takamiya Meguri', 'side');
addChar('Takashiro Natsume', 'side');
addChar('Yamanouchi Koharu', 'side');

export default vn;
