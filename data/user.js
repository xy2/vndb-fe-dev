import { ListStatus, ReleaseStatus } from '../lib/constants';

const user = {
  username: 'zebra',
  registeredDate: '2018-01-01',
  listStats: {
    vns: 34,
    releases: 1,
  },
  forumStats: {
    posts: 33,
    threads: 0,
  },
  voteStats: {
    count: 31,
    average: 5,
    distribution: [
      { score: 10, count: 2 },
      { score: 9, count: 5 },
      { score: 8, count: 8 },
      { score: 7, count: 9 },
      { score: 6, count: 4 },
      { score: 5, count: 2 },
      { score: 4, count: 0 },
      { score: 3, count: 1 },
      { score: 2, count: 0 },
      { score: 1, count: 0 },
    ],
  },
  listEntries: [
    {
      id: 88881,
      vn: {
        id: 97,
        title: 'Saya no Uta',
        originalTitle: '沙耶の唄',
        coverImage: '/content/images/gung_0005pl.jpg',
      },
      vote: 8,
      status: ListStatus.Finished,
      added: '2014-05-05',
      comment: 'Short lovecraftian horror story by Gen Urobuchi.',
    },
    {
      id: 88882,
      vn: {
        id: 5922,
        title: 'Kara no Shoujo - The Second Episode',
        originalTitle: '虚ノ少女',
        coverImage: '/content/images/gung_0005pl.jpg',
      },
      vote: 9,
      status: ListStatus.Finished,
      added: '2015-11-08',
      releases: [
        {
          id: 99991,
          release: {
            id: 41657,
            date: '2013-02-08',
            name: 'Kara no Shojo - The Second Episode - Trial Edition',
            platforms: ['web', 'linux', 'mac', 'win'],
          },
          status: ReleaseStatus.Obtained,
        },
      ],
    },
  ],
};

export default user;

user.voteStats.average =
  user.voteStats.distribution.reduce((a, x) => a + x.score * x.count, 0) /
  user.voteStats.count;
