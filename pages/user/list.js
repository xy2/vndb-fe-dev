import { Fragment, useState } from 'react';
import Head from 'next/head';
import Layout from '../../components/Layout';
import Card from '../../components/Card';
import user from '../../data/user';
import UserHeader from '../../components/UserHeader';
import VerticalSelector from '../../components/VerticalSelector';
import Button from '../../components/Button';
import ButtonGroup from '../../components/ButtonGroup';
import { GridIcon, ListIcon } from '../../components/icons';
import { ListStatus, ListStatusNames } from '../../lib/constants';
import Dropdown from '../../components/Dropdown';
import MoreButton from '../../components/MoreButton';
import { classes } from '../../lib/util';
import VnGrid from '../../components/VnGrid';
import CommonModal from '../../components/CommonModal';
import { prevent } from '../../lib/dom';

const categoryOptions = [
  { id: ListStatus.All },
  { id: ListStatus.Unknown },
  { id: ListStatus.Playing },
  { id: ListStatus.Finished },
  { id: ListStatus.Stalled },
  { id: ListStatus.Dropped },
];

categoryOptions.forEach((x) => {
  x.text = ListStatusNames[x.id];
});

const statusOptions = categoryOptions.slice(1);

export default function UserListPage() {
  const [selectedStatus, setSelectedStatus] = useState(ListStatus.All);

  const [editingEntry, setEditingEntry] = useState(null);

  const listEntries = user.listEntries.filter((x) => {
    return selectedStatus === ListStatus.All || x.status === selectedStatus;
  });

  function handleEntryEdit(entry) {
    setEditingEntry(entry);
  }

  function handleEntryRemove() {
    // nop
  }

  return (
    <Layout headerControls={<UserHeader user={user} page="list" />}>
      <Head>
        <title>{user.username} | vndb</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="main-container container">
        <div className="row">
          <div className="fixed-size-left-sidebar-xl">
            <div className="vertical-selector-label">Status</div>
            <VerticalSelector
              items={categoryOptions}
              selected={selectedStatus}
              onChange={setSelectedStatus}
            />
          </div>
          <div className="col-md">
            <div className="card card--white card--no-separators flex-expand mb-5">
              <div className="card__section">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Add a visual novel to your list..."
                />
              </div>
            </div>
            <UserListCard
              listEntries={listEntries}
              onEntryEdit={handleEntryEdit}
              onEntryRemove={handleEntryRemove}
            />
          </div>
        </div>
      </div>

      {editingEntry && (
        <EditEntryModal
          entry={editingEntry}
          onClose={() => setEditingEntry(null)}
        />
      )}
    </Layout>
  );
}

function UserListCard({ listEntries, onEntryEdit, onEntryRemove }) {
  const [display, setDisplay] = useState('list');

  return (
    <Card
      title="List"
      noSeparators
      headerButtons={
        <ButtonGroup>
          <Button active={display == 'list'} onClick={() => setDisplay('list')}>
            <ListIcon />
          </Button>
          <Button active={display == 'grid'} onClick={() => setDisplay('grid')}>
            <GridIcon />
          </Button>
        </ButtonGroup>
      }
    >
      {display == 'list' && (
        <VnList
          entries={listEntries}
          onEntryEdit={onEntryEdit}
          onEntryRemove={onEntryRemove}
        />
      )}
      <div className="card__body fs-medium stack">
        {display == 'grid' && (
          <VnGrid
            entries={listEntries}
            onEntryEdit={onEntryEdit}
            onEntryRemove={onEntryRemove}
          />
        )}
        <div className="d-lg-flex jc-between align-items-center">
          <div className="d-flex align-items-center"></div>
          <div className="d-block d-lg-none mb-2"></div>
          <div className="d-flex jc-right align-items-center">
            <Button disabled>&lt; Prev</Button>
            <div className="mx-3 semi-muted">page 1 of 3</div>
            <Button>Next &gt;</Button>
          </div>
        </div>
      </div>
    </Card>
  );
}

const vnListCols = [];
function VnList({ entries, onEntryEdit, onEntryRemove }) {
  return (
    <table className="table table--responsive-single-sm fs-medium vn-list">
      <thead>
        <tr>
          <th width="15%" className="th--nopad">
            <a
              href="#"
              className="table-header with-sort-icon with-sort-icon--active with-sort-icon--down"
              onClick={prevent}
            >
              Date
            </a>
          </th>
          <th width="40%" className="th--nopad">
            <a
              href="#"
              className="table-header with-sort-icon with-sort-icon--down"
              onClick={prevent}
            >
              Name
            </a>
          </th>
          <th width="10%" className="th--nopad">
            <a
              href="#"
              className="table-header with-sort-icon with-sort-icon--down"
              onClick={prevent}
            >
              Vote
            </a>
          </th>
          <th width="13%" className="th--nopad">
            <a
              href="#"
              className="table-header with-sort-icon with-sort-icon--down"
              onClick={prevent}
            >
              Status
            </a>
          </th>
          <th width="7.33%"></th>
          <th width="7.33%"></th>
          <th width="7.33%"></th>
        </tr>
      </thead>
      <tbody>
        {entries.map((entry) => (
          <VnListRow
            key={entry.id}
            entry={entry}
            onEntryEdit={() => onEntryEdit(entry)}
            onEntryRemove={() => onEntryRemove(entry)}
          />
        ))}
      </tbody>
    </table>
  );
}

function VnListRow({ entry, onEntryEdit, onEntryRemove }) {
  const [showCommentRow, setShowCommentRow] = useState(false);
  const [showReleasesRow, setShowReleasesRow] = useState(false);

  const menu = [
    { text: 'Edit', onClick: onEntryEdit },
    { text: 'Remove', onClick: onEntryRemove },
  ];

  const hasReleases = entry.releases && entry.releases.length > 0;

  return (
    <Fragment>
      <tr>
        <td className="tabular-nums muted">{entry.added}</td>
        <td>
          <a href="/v97" title={entry.originalTitle}>
            {entry.vn.title}
          </a>
        </td>
        <td className="table-edit-overlay-base">
          <input
            className="form-control form-control--table-edit form-control--stealth"
            value={entry.vote}
            onChange={(e) => (entry.vote = Number(e.target.value))}
          />
        </td>
        <td className="table-edit-overlay-base">
          <span className="js-table-edit-content">
            {ListStatusNames[entry.status]}
          </span>
          <select
            className="form-control form-control--table-edit form-control--table-edit-overlay"
            value={entry.status}
            onChange={(e) => (entry.status = Number(e.target.value))}
          >
            {statusOptions.map((x) => (
              <option key={x.id} value={x.id}>
                {x.text}
              </option>
            ))}
          </select>
        </td>
        <td>
          {hasReleases && (
            <a
              href="#"
              className="vn-list__expand-releases"
              onClick={(e) => {
                e.preventDefault();
                setShowReleasesRow(!showReleasesRow);
              }}
            >
              <span
                className={classes(
                  'expand-arrow mr-2',
                  showReleasesRow && 'expand-arrow--open'
                )}
              ></span>
              {entry.releases.length}/{entry.releases.length}
            </a>
          )}
        </td>
        <td>
          <a
            href="#"
            className={classes(
              'vn-list__expand-comment',
              !entry.comment && 'vn-list__expand-comment--empty'
            )}
            onClick={(e) => {
              e.preventDefault();
              setShowCommentRow(!showCommentRow);
            }}
          >
            <span
              className={classes(
                'expand-arrow mr-2',
                showCommentRow && 'expand-arrow--open'
              )}
            ></span>
            <img className="svg-icon" src="/content/icons/comment.svg" />
          </a>
        </td>
        <td>
          <div className="d-flex jc-end">
            <Dropdown menu={menu} anchor="bottom-right">
              <MoreButton />
            </Dropdown>
          </div>
        </td>
      </tr>
      {showCommentRow && (
        <tr className="vn-list__comment-row">
          <td colSpan="6">
            {entry.comment && (
              <div className="vn-list__comment ml-3">{entry.comment}</div>
            )}
            {!entry.comment && (
              <div className="vn-list__comment ml-3 muted">
                Add a comment...
              </div>
            )}
          </td>
        </tr>
      )}
      {hasReleases && showReleasesRow && (
        <tr className="vn-list__releases-row">
          <td colSpan="6">
            <div className="vn-list__releases">
              <table className="table table--responsive-single-sm ml-3">
                <tbody>
                  {entry.releases.map((release) => (
                    <VnListRowReleaseRow key={release.id} release={release} />
                  ))}
                </tbody>
              </table>
            </div>
          </td>
        </tr>
      )}
    </Fragment>
  );
}

function VnListRowReleaseRow({ release }) {
  const menu = [
    { text: 'Edit', onClick: () => {} },
    { text: 'Remove', onClick: () => {} },
  ];

  return (
    <tr>
      <td width="15%" className="tabular-nums muted pl-0">
        {release.release.date}
      </td>
      <td width="50%">
        <a href="/v97" title="沙耶の唄">
          {release.release.name}
        </a>
      </td>
      <td width="20%" className="table-edit-overlay-base">
        <span className="js-table-edit-content">Obtained</span>
        <select className="form-control form-control--table-edit form-control--table-edit-overlay">
          <option value="0">Unknown</option>
          <option value="1">Pending</option>
          <option value="2" selected>
            Obtained
          </option>
          <option value="3">On loan</option>
          <option value="4">Deleted</option>
        </select>
      </td>
      <td width="15%">
        <div className="d-flex jc-end">
          <Dropdown menu={menu} anchor="bottom-right">
            <MoreButton />
          </Dropdown>
        </div>
      </td>
    </tr>
  );
}

function EditEntryModal({ entry, onClose }) {
  return (
    <CommonModal open={true} onClose={onClose}>
      <CommonModal.Header title={entry.vn.title} />
      <CommonModal.Body>
        <div className="form-group row">
          <label for="lem-date" className="col col--1 col-form-label">
            Date
          </label>
          <div className="col col--3 col-form-value">2015-11-08</div>
        </div>
        <div className="form-group row">
          <label for="lem-vote" className="col col--1 col-form-label">
            Vote
          </label>
          <div className="col col--3">
            <input
              type="text"
              className="form-control"
              id="lem-vote"
              placeholder="0.0 - 10.0"
              value="9.5"
            />
          </div>
        </div>
        <div className="form-group row">
          <label for="lem-status" className="col col--1 col-form-label">
            Status
          </label>
          <div className="col col--3">
            <select className="form-control">
              <option value="0">Unknown</option>
              <option value="1">Playing</option>
              <option value="2" selected>
                Finished
              </option>
              <option value="3">Stalled</option>
              <option value="4">Dropped</option>
            </select>
          </div>
        </div>
      </CommonModal.Body>
      <CommonModal.Body>
        <div className="modal__section-title modal__section-title--solo">
          Releases
        </div>
      </CommonModal.Body>
      <table className="table table--responsive-single-sm fs-medium modal__table mb-2">
        <thead>
          <tr>
            <th width="25%">Date</th>
            <th width="50%">Name</th>
            <th width="25%"></th>
            <th width="0%"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className="tabular-nums muted">2003-12-26</td>
            <td>Kara no Shoujo - The Second Episode - First Press Edition</td>
            <td>
              <select className="form-control">
                <option value="0">Unknown</option>
                <option value="1">Pending</option>
                <option value="2" selected>
                  Obtained
                </option>
                <option value="3">On loan</option>
                <option value="4">Deleted</option>
              </select>
            </td>
            <td>
              <button
                type="button"
                className="btn btn--subtle btn--remove-row lem__release-remove"
              />
            </td>
          </tr>
          <tr>
            <td className="tabular-nums muted">2003-12-26</td>
            <td>Kara no Shoujo - The Second Episode - First Press Edition</td>
            <td>
              <select className="form-control">
                <option value="0">Unknown</option>
                <option value="1">Pending</option>
                <option value="2" selected>
                  Obtained
                </option>
                <option value="3">On loan</option>
                <option value="4">Deleted</option>
              </select>
            </td>
            <td>
              <button
                type="button"
                className="btn btn--subtle btn--remove-row lem__release-remove"
              />
            </td>
          </tr>
        </tbody>
      </table>
      <CommonModal.Body>
        <div className="modal__section-title">Notes</div>
        <textarea
          className="form-control"
          rows="4"
          maxlength="500"
          style={{ resize: 'vertical' }}
        ></textarea>
      </CommonModal.Body>
      <CommonModal.Footer
        buttons={
          <>
            <Button appearance="subtle" onClick={onClose}>
              Cancel
            </Button>
            <Button type="submit">Save</Button>
          </>
        }
      />
    </CommonModal>
  );
}
