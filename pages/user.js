import Head from 'next/head';
import Layout from '../components/Layout';
import Card from '../components/Card';
import user from '../data/user';
import VoteGraph from '../components/VoteGraph';
import UserHeader from '../components/UserHeader';

export default function UserPage() {
  return (
    <Layout headerControls={<UserHeader user={user} page="details" />}>
      <Head>
        <title>{user.username} | vndb</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="main-container container">
        <div className="container main-container main-container--single-col stack stack--lg">
          <UserStatsCard user={user} />
          <UserRecentListUpdatesCard user={user} />
          <UserRecentContributionsCard user={user} />
        </div>
      </div>
    </Layout>
  );
}

function UserStatsCard({ user }) {
  return (
    <Card body>
      <div className="user-stats">
        <div className="user-stats__left">
          <div className="card__title mb-4">Stats</div>
          <div className="big-stats mb-5">
            <a href="/user/list" className="big-stats__stat">
              <div className="d-sm-none">VNs</div>
              <div className="d-none d-sm-block">Visual Novels</div>
              <div className="big-stats__value">12</div>
            </a>
            <a href="#" className="big-stats__stat">
              Edits
              <div className="big-stats__value">3</div>
            </a>
            <a href="#" className="big-stats__stat">
              Tags
              <div className="big-stats__value">0</div>
            </a>
          </div>
          <div className="user-stats__text">
            <dl className="dl--horizontal">
              <dt>List stats</dt>
              <dd>
                {user.listStats.releases}{' '}
                {user.listStats.releases == 1 ? 'release' : 'releases'} of{' '}
                {user.listStats.vns}{' '}
                {user.listStats.vns == 1 ? 'visual novel' : 'visual novels'}
              </dd>
              <dt>Forum stats</dt>
              <dd>
                {user.forumStats.posts}{' '}
                {user.forumStats.posts == 1 ? 'post' : 'posts'}
                {', '}
                {user.forumStats.threads}{' '}
                {user.forumStats.threads == 1 ? 'new thread' : 'new threads'}{' '}
                <a href="/user/posts">Browse posts »</a>
              </dd>
              <dt>Registered</dt>
              <dd>{user.registeredDate}</dd>
            </dl>
          </div>
          <div className="d-md-none mb-4"></div>
        </div>
        <div className="user-stats__right">
          <div className="card__title mb-2">Vote distribution</div>
          <VoteGraph stats={user.voteStats.distribution} />
          <div className="final-text">
            {user.voteStats.count} votes total, average{' '}
            {user.voteStats.average.toFixed(2)}
          </div>
        </div>
      </div>
    </Card>
  );
}

function UserRecentListUpdatesCard({ user }) {
  return (
    <Card title="Recent list updates" noSeparators>
      <table className="table table--responsive-single-sm">
        <thead>
          <tr>
            <th width="15%">Date</th>
            <th width="50%">Name</th>
            <th width="10%">Vote</th>
            <th width="25%">Status</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className="tabular-nums muted">2003-12-26</td>
            <td>
              <a href="/v97" title="沙耶の唄">
                Saya no Uta
              </a>
            </td>
            <td>9</td>
            <td>Finished</td>
          </tr>
        </tbody>
      </table>
      <Card.Body>
        <a href="/user/list">View full list</a>
      </Card.Body>
    </Card>
  );
}

function UserRecentContributionsCard({ user }) {
  return (
    <Card title="Recent contributions" noSeparators>
      <table className="table table--responsive-single-sm">
        <thead>
          <tr>
            <th width="15%">Date</th>
            <th width="85%">Name</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className="tabular-nums muted">2003-12-26</td>
            <td>
              <a href="/v97" title="沙耶の唄">
                Saya no Uta
              </a>
            </td>
          </tr>
        </tbody>
      </table>
      <Card.Body>
        <a href="/user/contributions">View all</a>
      </Card.Body>
    </Card>
  );
}
