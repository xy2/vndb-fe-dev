import Head from 'next/head';
import Card from '../components/Card';
import Layout from '../components/Layout';

export default function Home() {
  return (
    <Layout>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="container main-container">
        <div className="stack">
          <a href="/component-library">Component library</a>
          <a href="/vn">VN page</a>
          <a href="/char">Character page</a>
          <a href="/user">User profile</a>
          <a href="/user/list">User VN list</a>
        </div>
      </div>
    </Layout>
  );
}
