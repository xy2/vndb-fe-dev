import { Fragment, useState } from 'react';
import Head from 'next/head';
import Layout from '../components/Layout';
import char from '../data/char';
import EditMenu from '../components/EditMenu';
import CharacterStats from '../components/CharacterStats';
import { uniqueC } from '../lib/util';
import CharacterCard from '../components/CharacterCard';
import SpoilerOptions from '../components/SpoilerOptions';
import Switch from '../components/Switch';

export default function CharPage() {
  const [filters, setFilters] = useState({
    spoilers: 0,
    sexual: false,
  });

  return (
    <Layout headerControls={<CharHeader char={char} />}>
      <Head>
        <title>{char.name} | vndb</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="main-container container">
        <div className="page-inner-controls">
          <SpoilerOptions
            spoilers={filters.spoilers}
            onChange={(spoilers) => setFilters({ ...filters, spoilers })}
          />
          <div>
            <Switch
              text="Sexual tags"
              checked={filters.sexual}
              onToggle={() =>
                setFilters({ ...filters, sexual: !filters.sexual })
              }
            />
          </div>
        </div>
        <div className="row">
          <div className="fixed-size-left-sidebar-md">
            <img
              className="img img--fit d-none d-md-block detail-header-image-push"
              src={char.image}
            />
          </div>
          <div className="col-md">
            <div className="description serif mb-5">
              <p dangerouslySetInnerHTML={{ __html: char.descriptionHtml }} />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="fixed-size-left-sidebar-md">
            <h2 className="detail-page-sidebar-section-header">Details</h2>
          </div>
          <div className="col-md">
            <div className="card card--white mb-5">
              <div className="card__section">
                <CharacterStats character={char} filters={filters} />
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="fixed-size-left-sidebar-md">
            <h2 className="detail-page-sidebar-section-header">
              Visual novels
            </h2>
          </div>
          <div className="col-md">
            <CharVnCard char={char} />
          </div>
        </div>
        <div className="row">
          <div className="fixed-size-left-sidebar-md">
            <h2 className="detail-page-sidebar-section-header">Voiced by</h2>
          </div>
          <div className="col-md">
            <CharVoiceCard char={char} />
          </div>
        </div>
        <div className="row">
          <div className="fixed-size-left-sidebar-md">
            <h2 className="detail-page-sidebar-section-header">
              Other instances
            </h2>
          </div>
          <div className="col-md stack stack--sm">
            {char.otherInstances.map((other) => (
              <CharacterCard key={other.id} char={other} />
            ))}
          </div>
        </div>
      </div>
    </Layout>
  );
}

function CharHeader({ char }) {
  return (
    <div className="row">
      <div className="fixed-size-left-sidebar-md">
        <img
          className="page-header-img-mobile img img--rounded d-md-none"
          src={char.image}
        />
        <div className="detail-header-image-container">
          <img
            className="img img--fit img--rounded elevation-1 d-none d-md-block detail-header-image"
            src={char.image}
          />
        </div>
      </div>
      <div className="col-md">
        <div className="pull-right">
          <CharEditMenu char={char} />
        </div>
        <div className="detail-page-title">{char.name} ♂</div>
        <div className="detail-page-subtitle">{char.originalName}</div>
      </div>
    </div>
  );
}

function CharEditMenu({ char }) {
  const menu = [
    { text: 'Details', href: `/c${char.id}` },
    { text: 'History', href: `/c${char.id}/hist` },
    { text: 'Edit', href: `/c${char.id}/edit` },
  ];

  return <EditMenu menu={menu} anchor="bottom-right" />;
}

function CharVnCard({ char }) {
  return (
    <div className="card card--white mb-5">
      <table className="table table--responsive-single-sm">
        <thead>
          <tr>
            <th width="15%">Date</th>
            <th width="60%">Title</th>
            <th width="25%">Role</th>
          </tr>
        </thead>
        <tbody>
          {char.vns.map((vnEntry) => (
            <tr key={vnEntry.vn.id}>
              <td className="tabular-nums muted">{vnEntry.vn.releaseDate}</td>
              <td>
                <a href={'/v' + vnEntry.vn.id} title={vnEntry.vn.originalTitle}>
                  {vnEntry.vn.title}
                </a>
              </td>
              <td>{vnEntry.role}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

function CharVoiceCard({ char }) {
  const voices = uniqueC(
    [].concat(...char.vns.map((x) => x.voicedBy)),
    (a, b) => a.staff.id == b.staff.id && a.note == b.note
  );

  return (
    <div className="card card--white mb-5">
      <table className="table table--responsive-single-sm">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {voices.map((voice, i) => (
            <tr key={i}>
              <td>
                <a
                  href={'/s' + voice.staff.id}
                  title={voice.staff.originalName}
                >
                  {voice.staff.name}
                </a>
                {voice.note && ' '}
                {voice.note && <span>{voice.note}</span>}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
