import Head from 'next/head';
import Layout from '../components/Layout';
import { samples } from '../components/samples';
import { DocList, DocListSection, DocListLink } from '../components/DocList';

export default function Home() {
  return (
    <Layout>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="container main-container">
        <div className="vn-page">
          <div className="main-container container">
            <div className="row">
              <div className="fixed-size-left-sidebar-md">
                <DocList>
                  <DocListSection title="Components">
                    {samples.map((Sample, i) => (
                      <DocListLink key={i} href={'#' + Sample.name}>
                        {Sample.name.replace(/^Sample/, '')}
                      </DocListLink>
                    ))}
                  </DocListSection>
                </DocList>
              </div>
              <div className="col-md stack">
                <h1>Component library</h1>

                {samples.map((Sample, i) => (
                  <div key={i} id={Sample.name}>
                    <Sample key={i} />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
