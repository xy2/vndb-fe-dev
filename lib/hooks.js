import { useState, useEffect } from 'react';

export function useViewportWidth() {
  const [width, setWidth] = useState(
    typeof window != 'undefined' ? window.innerWidth : 0
  );
  const [height, setHeight] = useState(
    typeof window != 'undefined' ? window.innerHeight : 0
  );

  useEffect(() => {
    const handleWindowResize = () => {
      setWidth(window.innerWidth);
      setHeight(window.innerHeight);
    };
    window.addEventListener('resize', handleWindowResize);

    // Return a function from the effect that removes the event listener
    return () => window.removeEventListener('resize', handleWindowResize);
  }, []);

  return { width, height };
}
