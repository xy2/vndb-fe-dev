export function genderSymbol(gender) {
  switch (gender) {
    case 'M':
      return '♂';
    case 'F':
      return '♀';
    case 'B':
      return '♀♂';
    default:
      return '';
  }
}
