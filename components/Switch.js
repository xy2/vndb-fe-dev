export default function Switch({ checked, onToggle, text }) {
  return (
    <button
      type="button"
      className="switch"
      aria-checked={checked}
      onClick={onToggle}
    >
      {text}
    </button>
  );
}
