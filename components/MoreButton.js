import { classes } from '../lib/util';

export default function MoreButton({ href, light, onClick }) {
  function handleClick(e) {
    if (!href) {
      e.preventDefault();
    }
    if (onClick) {
      onClick(e);
    }
  }
  return (
    <a
      href={href || '#'}
      className={classes('more-button', {
        'more-button--light': light,
      })}
      onClick={handleClick}
    >
      <span className="more-button__dots" />
    </a>
  );
}
