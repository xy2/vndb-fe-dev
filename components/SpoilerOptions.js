import Dropdown from './Dropdown';

export default function SpoilerOptions({ spoilers, onChange }) {
  const items = [
    { text: 'No spoilers', value: 0 },
    { text: 'Minor spoilers', value: 1 },
    { text: 'Spoil me!', value: 2 },
  ];
  const menu = items.map((item) => ({
    text: item.text,
    onClick: () => onChange(item.value),
  }));

  const item = items.find((x) => x.value == spoilers);

  return (
    <Dropdown className="tag-summary__option" menu={menu}>
      <a href="#" className="link--subtle">
        {item && item.text} <span className="caret"></span>
      </a>
    </Dropdown>
  );
}
