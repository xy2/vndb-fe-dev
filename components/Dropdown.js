import React, { useState, useEffect } from 'react';
import { classes } from '../lib/util';

export default function Dropdown({ menu, children, anchor, menuClass }) {
  const [open, setOpen] = useState(false);

  // cancel when clicking outside
  useEffect(() => {
    if (open) {
      const cancel = () => setOpen(false);
      document.addEventListener('click', cancel);
      return () => document.removeEventListener('click', cancel);
    }
  }, [open]);

  return (
    <div className={classes('dropdown', open && 'dropdown--open')}>
      <div
        onClick={(e) => {
          e.preventDefault();
          setOpen(!open);
        }}
      >
        {React.Children.only(children)}
      </div>
      <div
        className={classes(
          'dropdown-menu',
          'dropdown-menu--' + anchor,
          menuClass
        )}
      >
        {menu.map((x, i) => (
          <a
            key={i}
            className="dropdown-menu__item"
            href={x.href || '#'}
            onClick={(e) => click(x, e)}
          >
            {x.text}
          </a>
        ))}
      </div>
    </div>
  );
}

function click(menuItem, event) {
  if (!menuItem.href) {
    event.preventDefault();
  }
  if (menuItem.onClick) {
    menuItem.onClick(event);
  }
}
