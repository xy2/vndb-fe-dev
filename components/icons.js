function IconBase({ path, children }) {
  return (
    <svg
      className="svg-icon"
      xmlns="http://www.w3.org/2000/svg"
      width="14"
      height="14"
      version="1"
    >
      <g fill="currentColor" fillRule="nonzero">
        {path && <path d={path}></path>}
        {children}
      </g>
    </svg>
  );
}

export function ListIcon() {
  return <IconBase path="M0 2h14v2H0zM0 6h14v2H0zM0 10h14v2H0z" />;
}

export function GridIcon() {
  return (
    <IconBase path="M0 0h3v3H0zM0 5h3v3H0zM0 10h3v3H0zM5 0h3v3H5zM5 5h3v3H5zM5 10h3v3H5zM10 0h3v3h-3zM10 5h3v3h-3zM10 10h3v3h-3z" />
  );
}
