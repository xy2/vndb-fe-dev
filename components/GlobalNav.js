import React from 'react';
import Dropdown from './Dropdown';

export default function GlobalNav() {
  const databaseMenu = [
    { text: 'Visual novels', href: '/v/all' },
    { text: 'Tags', href: '/g' },
    { text: 'Characters', href: '/c/all' },
    { text: 'Traits', href: '/i' },
    { text: 'Producers', href: '/p/all' },
    { text: 'Staff', href: '/s/all' },
    { text: 'Releases', href: '/r' },
  ];
  const contributeMenu = [
    { text: 'Recent changes', href: '/hist' },
    { text: 'Add Visual Novel', href: '/v/add' },
    { text: 'Add Producer', href: '/p/add' },
    { text: 'Add Staff', href: '/s/new' },
    { text: 'Add Character', href: '/c/new' },
  ];

  return (
    <div className="navbar navbar--expand-md">
      <div className="navbar__logo">
        <a href="/">vndb</a>
      </div>
      <a href="#" className="navbar__toggler">
        <div className="navbar__toggler-icon"></div>
      </a>
      <div className="navbar__collapse">
        <div className="nav navbar__nav navbar__main-nav">
          <Dropdown menu={databaseMenu} menuClass="database-menu">
            <div className="nav__item navbar__menu">
              <a href="#" className="nav__link dropdown__toggle">
                Database <span className="caret"></span>
              </a>
            </div>
          </Dropdown>
          <div className="nav__item navbar__menu">
            <a className="nav__link" href="/d6">
              FAQ
            </a>
          </div>
          <div className="nav__item navbar__menu">
            <a className="nav__link" href="/t">
              Forums
            </a>
          </div>
          <Dropdown menu={contributeMenu}>
            <div className="nav__item navbar__menu">
              <a href="#" className="nav__link dropdown__toggle">
                Contribute <span className="caret"></span>
              </a>
            </div>
          </Dropdown>
          <div className="nav__item navbar__menu">
            <a href="/v/all" className="nav__link">
              <span className="icon-desc d-md-none">Search </span>
              <img className="svg-icon" src="/content/icons/search.svg" />
            </a>
          </div>
        </div>
        <div className="nav navbar__nav">
          <div className="nav__item navbar__menu">
            <a className="nav__link" href="/u/register">
              Register
            </a>
          </div>
          <div className="nav__item navbar__menu">
            <a className="nav__link" href="/u/login">
              Login
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
