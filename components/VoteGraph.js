export default function VoteGraph({ stats }) {
  const max = Math.max(...stats.map((x) => x.count));

  return (
    <div className="vote-graph">
      {stats.map((stat, i) => (
        <div
          key={i}
          className="vote-graph__bar"
          data-score={stat.score}
          data-count={stat.count}
          style={{ width: (stat.count / max) * 100 + '%' }}
        />
      ))}
    </div>
  );
}
