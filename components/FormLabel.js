import { useContext } from 'react';
import InputContext from '../lib/input-context';

export default function FormLabel({ htmlFor, children }) {
  const context = useContext(InputContext);
  return (
    <label htmlFor={htmlFor || (context && context.controlId)}>
      {children}
    </label>
  );
}
