export default function StaffCredits({ groups }) {
  return (
    <div className="staff-credits">
      {groups.map((group, i) => (
        <div key={i} className="staff-credits__section">
          <div className="staff-credits__section-title">{group.name}</div>
          {group.credits.map((credit, i) => (
            <div key={i} className="staff-credits__item">
              <a href={'/s' + credit.staff.id}>{credit.staff.name}</a>{' '}
              <span className="staff-credits__note">{credit.note}</span>
            </div>
          ))}
        </div>
      ))}
    </div>
  );
}
