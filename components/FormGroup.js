import { useMemo } from 'react';
import FormLabel from './FormLabel';
import InputContext from '../lib/input-context';

export default function FormGroup({ controlId, label, help, error, children }) {
  const value = useMemo(() => ({ controlId }), [controlId]);
  return (
    <InputContext.Provider value={value}>
      <div className="form-group">
        {label && <FormLabel>{label}</FormLabel>}
        {children}
        {help && <FormGroup.Help>{help}</FormGroup.Help>}
        {error && <FormGroup.Error>{error}</FormGroup.Error>}
      </div>
    </InputContext.Provider>
  );
}

FormGroup.Help = function FormGroupHelp({ children }) {
  return <div className="form-group__help">{children}</div>;
};

FormGroup.Error = function FormGroupError({ children }) {
  return <div className="invalid-feedback">{children}</div>;
};
