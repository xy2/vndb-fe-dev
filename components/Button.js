import { classes } from '../lib/util';

export default function Button({
  appearance,
  active,
  href,
  type,
  caret,
  children,
  onClick,
}) {
  let classname = classes({
    btn: true,
    'btn--subtle': appearance === 'subtle',
    active: active,
  });

  if (href) {
    return (
      <a href={href} className={classname}>
        {children}
      </a>
    );
  }

  return (
    <button type={type || 'button'} className={classname} onClick={onClick}>
      {children}
      {caret && ' '}
      {caret && <span className="caret" />}
    </button>
  );
}
