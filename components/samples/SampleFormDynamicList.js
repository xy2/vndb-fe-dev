export default function SampleFormDynamicList() {
  return (
    <div>
      <h2>Form dynamic list</h2>
      <div className="example">
        <div className="editable-list editable-list--sm">
          <div className="editable-list__row row row--compact ">
            <div className="editable-list__field col-sm col-sm--1 col-form-label single-line">
              <a href="/s390" target="_blank">
                Hashimoto Miyuki
              </a>
            </div>
            <div className="editable-list__field col-sm col-sm--1">
              <select className="form-control">
                <option value="scenario">Scenario</option>
                <option value="chardesign">Character design</option>
                <option value="art">Artist</option>
                <option value="music">Composer</option>
                <option value="songs">Vocals</option>
                <option value="director">Director</option>
                <option value="staff">Staff</option>
              </select>
            </div>
            <div className="editable-list__field col-sm col-sm--2">
              <input
                type="text"
                className="form-control"
                maxLength="250"
                placeholder="Note"
              />
            </div>
            <div className="editable-list__field col-sm col-sm--auto">
              <button type="button" className="btn">
                <span className="d-none d-sm-inline">x</span>
                <span className="d-sm-none">Remove</span>
              </button>
            </div>
          </div>
          <div className="editable-list__row row row--compact ">
            <div className="editable-list__field col-sm col-sm--1 col-form-label single-line">
              <a href="/s62" target="_blank">
                Tachibana Aya
              </a>
            </div>
            <div className="editable-list__field col-sm col-sm--1">
              <select className="form-control">
                <option value="scenario">Scenario</option>
                <option value="chardesign">Character design</option>
                <option value="art">Artist</option>
                <option value="music">Composer</option>
                <option value="songs">Vocals</option>
                <option value="director">Director</option>
                <option value="staff">Staff</option>
              </select>
            </div>
            <div className="editable-list__field col-sm col-sm--2">
              <input
                type="text"
                className="form-control"
                maxLength="250"
                placeholder="Note"
              />
            </div>
            <div className="editable-list__field col-sm col-sm--auto">
              <button type="button" className="btn">
                <span className="d-none d-sm-inline">x</span>
                <span className="d-sm-none">Remove</span>
              </button>
            </div>
          </div>
          <div className="editable-list__row row row--compact ">
            <div className="editable-list__field col-sm col-sm--1 col-form-label single-line">
              <a href="/s1442" target="_blank">
                CooRie
              </a>
            </div>
            <div className="editable-list__field col-sm col-sm--1">
              <select className="form-control">
                <option value="scenario">Scenario</option>
                <option value="chardesign">Character design</option>
                <option value="art">Artist</option>
                <option value="music">Composer</option>
                <option value="songs">Vocals</option>
                <option value="director">Director</option>
                <option value="staff">Staff</option>
              </select>
            </div>
            <div className="editable-list__field col-sm col-sm--2">
              <input
                type="text"
                className="form-control"
                maxLength="250"
                placeholder="Note"
              />
            </div>
            <div className="editable-list__field col-sm col-sm--auto">
              <button type="button" className="btn">
                <span className="d-none d-sm-inline">x</span>
                <span className="d-sm-none">Remove</span>
              </button>
            </div>
          </div>
        </div>
        <div className="form-group">
          <label htmlFor="add-staff">Add staff</label>
          <div className="form-control-wrap" style={{ maxWidth: '400px' }}>
            <input
              type="text"
              className="form-control"
              id="add-staff"
              name="add-staff"
            />
          </div>
        </div>
      </div>
    </div>
  );
}
