export default function SampleDescriptionList() {
  return (
    <div>
      <h2>Description list</h2>
      <h3>Vertical</h3>
      <div className="example">
        <dl>
          <dt>Original Title</dt>
          <dd>虚ノ少女</dd>
          <dt>Main Title</dt>
          <dd>Kara no Shoujo - The Second Episode</dd>
          <dt>Aliases</dt>
          <dd>Kara no Shoujo 2, 殻2, ウロ, Uro no Shoujo, KnS2</dd>
        </dl>
      </div>
      <h3>Horizontal</h3>
      <div className="example">
        <dl className="dl--horizontal">
          <dt>List stats</dt>
          <dd>0 releases of 0 visual novels</dd>
          <dt>Forum stats</dt>
          <dd>682 posts, 84 new threads</dd>
          <dt>Registered</dt>
          <dd>2019-09-20</dd>
        </dl>
      </div>
    </div>
  );
}
