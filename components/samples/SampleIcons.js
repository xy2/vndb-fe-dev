import ReleaseList from '../ReleaseList';
import PlatformIcons from '../PlatformIcons';

const platforms = [
  'web',
  'lin',
  'mac',
  'win',
  'ios',
  'and',
  'dvd',
  'bdp',
  'fmt',
  'gba',
  'gbc',
  'msx',
  'nds',
  'nes',
  'p88',
  'p98',
  'pce',
  'pcf',
  'psp',
  'ps1',
  'ps2',
  'ps3',
  'ps4',
  'psv',
  'drc',
  'sat',
  'sfc',
  'swi',
  'wii',
  'wiu',
  'n3d',
  'x68',
  'xb1',
  'xb3',
  'xbo',
  'oth',
];

export default function SampleIcons() {
  return (
    <div>
      <h2>Icons</h2>
      <div className="example">
        <div className="relsm__rel-col relsm__rel-platforms icon-set">
          <PlatformIcons platforms={platforms} />
        </div>
      </div>
    </div>
  );
}
