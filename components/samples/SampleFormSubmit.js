import Button from '../Button';

export default function SampleFormSubmit() {
  return (
    <div>
      <h2>Form submit</h2>
      <div className="example stack">
        <div className="stack stack--right">
          <Button type="submit">Submit</Button>
        </div>
        <div className="stack stack--right">
          <Button type="submit">Submit</Button>
          <div className="invalid-feedback">Missing CSRF token.</div>
        </div>
      </div>
    </div>
  );
}
