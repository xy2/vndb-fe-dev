export default function SampleSelector() {
  return (
    <div>
      <h2>Selector</h2>
      <h3>Vertical selector</h3>
      <div className="example">
        <div className="vertical-selector">
          <a
            href="#"
            className="vertical-selector__item vertical-selector__item--active"
          >
            All
          </a>
          <a href="#" className="vertical-selector__item">
            Unknown
          </a>
          <a href="#" className="vertical-selector__item">
            Playing
          </a>
          <a href="#" className="vertical-selector__item">
            Finished
          </a>
          <a href="#" className="vertical-selector__item">
            Stalled
          </a>
          <a href="#" className="vertical-selector__item">
            Dropped
          </a>
        </div>
      </div>
    </div>
  );
}
