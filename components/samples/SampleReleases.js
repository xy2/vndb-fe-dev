import ReleaseList from '../ReleaseList';
import vn from '../../data/vn';

export default function SampleReleases() {
  return (
    <div>
      <h2>Releases</h2>
      <div className="example">
        <ReleaseList releases={vn.releaseData} />
        <a href={`/${vn.id}/releases`}>Compare releases</a>
      </div>
    </div>
  );
}
