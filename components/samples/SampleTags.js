import TagSummary from '../TagSummary';

export default function SampleTags() {
  return (
    <div>
      <h2>Tags</h2>
      <div className="example">
        <TagSummary tags={tags} />
      </div>
    </div>
  );
}

const tags = [
  {
    id: 1471,
    type: 'tech',
    spoil: 0,
    frac: 1,
    name: 'Blind Choices',
  },
  {
    id: 459,
    type: 'tech',
    spoil: 0,
    frac: 1,
    name: 'Unlockable Routes',
  },
  {
    id: 19,
    type: 'cont',
    spoil: 0,
    frac: 1,
    name: 'Mystery',
  },
  {
    id: 553,
    type: 'tech',
    spoil: 0,
    frac: 1,
    name: 'Multiple Route Mystery',
  },
  {
    id: 981,
    type: 'tech',
    spoil: 0,
    frac: 1,
    name: 'Passage of Time',
  },
  {
    id: 323,
    type: 'cont',
    spoil: 0,
    frac: 0.93,
    name: 'Detective Work',
  },
  {
    id: 218,
    type: 'cont',
    spoil: 1,
    frac: 0.93,
    name: 'Traditionalist Family',
  },
  {
    id: 1646,
    type: 'tech',
    spoil: 2,
    frac: 0.93,
    name: 'Varied Title Screens',
  },
  {
    id: 153,
    type: 'cont',
    spoil: 0,
    frac: 0.9,
    name: 'Murder Mystery',
  },
  {
    id: 181,
    type: 'tech',
    spoil: 0,
    frac: 0.9,
    name: 'One True End',
  },
  {
    id: 1229,
    type: 'cont',
    spoil: 0,
    frac: 0.9,
    name: 'Heroine with Kimono',
  },
  {
    id: 676,
    type: 'cont',
    spoil: 0,
    frac: 0.9,
    name: 'Flashback',
  },
  {
    id: 992,
    type: 'ero',
    spoil: 2,
    frac: 0.9,
    name: 'Inbreeding',
  },
  {
    id: 848,
    type: 'cont',
    spoil: 2,
    frac: 0.9,
    name: 'Death of Heroine',
  },
  {
    id: 373,
    type: 'cont',
    spoil: 0,
    frac: 0.9,
    name: 'Student Heroine',
  },
  {
    id: 497,
    type: 'cont',
    spoil: 2,
    frac: 0.9,
    name: 'Heroine with Psychological Problems',
  },
  {
    id: 1059,
    type: 'tech',
    spoil: 0,
    frac: 0.87,
    name: 'Open Ending(s)',
  },
  {
    id: 757,
    type: 'cont',
    spoil: 0,
    frac: 0.87,
    name: 'Winter',
  },
  {
    id: 591,
    type: 'cont',
    spoil: 0,
    frac: 0.87,
    name: 'Private Detective Protagonist',
  },
  {
    id: 322,
    type: 'cont',
    spoil: 0,
    frac: 0.87,
    name: 'Crime',
  },
  {
    id: 672,
    type: 'tech',
    spoil: 2,
    frac: 0.87,
    name: 'Bad Ending(s)',
  },
  {
    id: 1474,
    type: 'tech',
    spoil: 0,
    frac: 0.87,
    name: 'Ending List',
  },
];
