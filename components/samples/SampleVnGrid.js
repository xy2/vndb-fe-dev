import VnGrid from '../VnGrid';
import user from '../../data/user';

export default function SampleVnGrid() {
  return (
    <div>
      <h2>VN grid</h2>
      <VnGrid entries={user.listEntries} />
    </div>
  );
}
