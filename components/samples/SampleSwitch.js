import Switch from '../Switch';
import { useState } from 'react';

export default function SampleSwitch() {
  const [a, setA] = useState(false);
  const [b, setB] = useState(true);

  return (
    <div>
      <h2>Switch</h2>
      <div className="example stack stack--left">
        <Switch text="Content" checked={a} onToggle={() => setA(!a)} />
        <Switch text="Content" checked={b} onToggle={() => setB(!b)} />
      </div>
    </div>
  );
}
