export default function SampleStack() {
  return (
    <div>
      <h2>Stack</h2>
      <p>Stacks provide uniform spacing between their children.</p>
      <div className="example stack">
        <div className="example-border stack">
          <div className="example-size">stack--md</div>
          <div className="example-size">stack--md</div>
        </div>

        <div className="example-border stack stack--sm">
          <div className="example-size">stack--sm</div>
          <div className="example-size">stack--sm</div>
        </div>

        <div className="example-border stack stack--xs">
          <div className="example-size">stack--xs</div>
          <div className="example-size">stack--xs</div>
        </div>
      </div>
    </div>
  );
}
