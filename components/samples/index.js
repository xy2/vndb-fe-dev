import SampleTypography from './SampleTypography';
import SampleButton from './SampleButton';
import SampleCard from './SampleCard';
import SampleCardForm from './SampleCardForm';
import SampleDescriptionList from './SampleDescriptionList';
import SampleDocList from './SampleDocList';
import SampleDropdown from './SampleDropdown';
import SampleFormControls from './SampleFormControls';
import SampleFormDynamicList from './SampleFormDynamicList';
import SampleFormGroup from './SampleFormGroup';
import SampleFormSubmit from './SampleFormSubmit';
import SampleGallery from './SampleGallery';
import SampleGrid from './SampleGrid';
import SampleHeaderComponents from './SampleHeaderComponents';
import SampleReleases from './SampleReleases';
import SampleIcons from './SampleIcons';
import SampleSelector from './SampleSelector';
import SampleStack from './SampleStack';
import SampleSwitch from './SampleSwitch';
import SampleTable from './SampleTable';
import SampleTags from './SampleTags';
import SampleVnGrid from './SampleVnGrid';
import SampleVoteGraph from './SampleVoteGraph';

export const samples = [
  SampleTypography,
  SampleButton,
  SampleCard,
  SampleCardForm,
  SampleDescriptionList,
  SampleDocList,
  SampleDropdown,
  SampleFormControls,
  SampleFormDynamicList,
  SampleFormGroup,
  SampleFormSubmit,
  SampleGallery,
  SampleGrid,
  SampleHeaderComponents,
  SampleReleases,
  SampleIcons,
  SampleSelector,
  SampleStack,
  SampleSwitch,
  SampleTable,
  SampleTags,
  SampleVnGrid,
  SampleVoteGraph,
];
