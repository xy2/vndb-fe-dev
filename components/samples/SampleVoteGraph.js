import VoteGraph from '../VoteGraph';

export default function SampleVoteGraph() {
  return (
    <div>
      <h2>Vote graph</h2>
      <div className="example">
        <VoteGraph stats={voteStats} />
      </div>
    </div>
  );
}

const voteStats = [
  { score: 10, count: 293 },
  { score: 9, count: 601 },
  { score: 8, count: 402 },
  { score: 7, count: 165 },
  { score: 6, count: 52 },
  { score: 5, count: 21 },
  { score: 4, count: 13 },
  { score: 3, count: 6 },
  { score: 2, count: 4 },
  { score: 1, count: 2 },
];
