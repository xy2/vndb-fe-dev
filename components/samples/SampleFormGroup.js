import FormGroup from '../FormGroup';
import FormLabel from '../FormLabel';
import Input from '../Input';

export default function SampleFormGroup() {
  return (
    <div>
      <h2>Form group</h2>
      <div className="example">
        <FormGroup label="Form group example" controlId="blah">
          <Input />
        </FormGroup>
        <FormGroup
          label="Form group example"
          help="Help text for form group"
          error="This field is required."
          controlId="blah"
        >
          <Input />
        </FormGroup>
        <FormGroup controlId="blah">
          <FormLabel>Form group example</FormLabel>
          <Input />
          <FormGroup.Help>Help text for form group</FormGroup.Help>
          <FormGroup.Error>This field is required.</FormGroup.Error>
        </FormGroup>
      </div>
    </div>
  );
}
