import { DocList, DocListLink, DocListSection } from '../DocList';

export default function SampleDocList() {
  return (
    <div>
      <h2>Doc list</h2>
      <div className="example">
        <DocList>
          <DocListSection title="About VNDB">
            <DocListLink href="/d7" active>
              About us
            </DocListLink>
            <DocListLink href="/d6">FAQ</DocListLink>
            <DocListLink href="/d9">Discussion board</DocListLink>
            <DocListLink href="/d17">
              Privacy Policy &amp; Licensing
            </DocListLink>
            <DocListLink href="/d11">Database API</DocListLink>
            <DocListLink href="/d14">Database Dumps</DocListLink>
            <DocListLink href="/d18">Database Querying</DocListLink>
            <DocListLink href="/d8">Development</DocListLink>
          </DocListSection>
          <DocListSection title="Guidelines">
            <DocListLink href="/d5">Editing guidelines</DocListLink>
            <DocListLink href="/d2">Visual novels</DocListLink>
            <DocListLink href="/d15">Special games</DocListLink>
            <DocListLink href="/d3">Releases</DocListLink>
            <DocListLink href="/d4">Producers</DocListLink>
            <DocListLink href="/d16">Staff</DocListLink>
            <DocListLink href="/d12">Characters</DocListLink>
            <DocListLink href="/d10">Tags &amp; Traits</DocListLink>
            <DocListLink href="/d13">Capturing screenshots</DocListLink>
          </DocListSection>
        </DocList>
      </div>
    </div>
  );
}
