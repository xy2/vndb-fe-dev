import Card from '../Card';

export default function SampleCardForm() {
  return (
    <div>
      <h2>Card form</h2>
      <p>
        You can use grid + card__form-section-left + card__form-section-title +
        form groups to place a form within a card.
      </p>
      <div className="example stack">
        <Card title="Card form">
          <Card.Section>
            <div className="row">
              <div className="col-md col-md--1 card__form-section-left">
                <label htmlFor="title-1" className="card__form-section-title">
                  Title
                </label>
              </div>
              <div className="col-md col-md--2">
                <input type="text" className="form-control" name="title-1" />
              </div>
            </div>
          </Card.Section>
          <Card.Section>
            <div className="row">
              <div className="col-md col-md--1 card__form-section-left">
                <div className="card__form-section-title">Title</div>
              </div>
              <div className="col-md col-md--2">
                <div className="form-group">
                  <label htmlFor="title-2">Title (romaji)</label>
                  <input type="text" className="form-control" name="title-2" />
                </div>
              </div>
            </div>
          </Card.Section>
          <Card.Section>
            <div className="row">
              <div className="col-md col-md--1 card__form-section-left">
                <div className="card__form-section-title">Title</div>
              </div>
              <div className="col-md col-md--2">
                <div className="form-group">
                  <label htmlFor="title-3">Title (romaji)</label>
                  <input type="text" className="form-control" name="title-3" />
                </div>
                <div className="form-group">
                  <label htmlFor="original">Original</label>
                  <input type="text" className="form-control" name="original" />
                  <div className="form-group__help">
                    The original title of this visual novel, leave blank if it
                    already is in the Latin alphabet.
                  </div>
                </div>
              </div>
            </div>
          </Card.Section>
          <Card.Section>
            <div className="d-flex jc-end">
              <input type="submit" className="btn" value="Submit" />
            </div>
          </Card.Section>
        </Card>
        <div className="d-flex jc-end">
          <input type="submit" className="btn" value="Or, Submit" />
        </div>
      </div>
    </div>
  );
}
