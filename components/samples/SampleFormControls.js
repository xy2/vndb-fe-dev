import Select from '../Select';
import Input from '../Input';
import Checkbox from '../Checkbox';

export default function SampleFormControls() {
  return (
    <div>
      <h2>Form controls</h2>
      <div className="example stack">
        <Input />
        <Select options={['First', 'Second', 'Third']} />
        <Checkbox label="I'm a checkbox" />
        <Input value="I'm invalid" invalid onChange={() => {}} />
      </div>
    </div>
  );
}
