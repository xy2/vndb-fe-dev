import { classes } from '../lib/util';

export function DocList({ children }) {
  return <nav className="doc-list">{children}</nav>;
}

export function DocListSection({ title, children }) {
  return (
    <>
      {title && <div className="doc-list__title">{title}</div>}
      {children}
    </>
  );
}

export function DocListLink({ href, active, children }) {
  return (
    <a
      className={classes('doc-list__doc', active && 'doc-list__doc--active')}
      href={href}
    >
      {children}
    </a>
  );
}
