import { useCallback, useEffect, useState } from 'react';
import { prevent } from '../lib/dom';

export default function Lightbox({
  initialIndex = 0,
  images,
  isOpen,
  onClose,
}) {
  const [index, setIndex] = useState(null);
  const move = useCallback(
    (amount) => {
      setIndex(Math.max(Math.min(index + amount, images.length - 1), 0));
    },
    [index, images]
  );

  const handleKeydown = useCallback(
    (e) => {
      if (e.keyCode == 37) move(-1);
      if (e.keyCode == 39) move(1);
      if (e.keyCode == 27) onClose();
    },
    [move, onClose]
  );

  useEffect(() => {
    if (isOpen) {
      document.body.classList.add('lightbox-open');
      document.addEventListener('keydown', handleKeydown);
      return () => {
        document.body.classList.remove('lightbox-open');
        document.removeEventListener('keydown', handleKeydown);
      };
    }
  }, [isOpen, handleKeydown]);

  useEffect(() => {
    if (!isOpen) {
      setIndex(null);
    }
    if (isOpen && index === null) {
      setIndex(initialIndex);
    }
  }, [isOpen, index]);

  if (!isOpen || index === null) {
    return null;
  }

  const handleClose = (e) => {
    e.preventDefault();
    onClose();
  };

  const handleBackdropClick = (e) => {
    if (e.target.classList.contains('lightbox')) {
      handleClose(e);
    }
  };

  const preventAndMove = (e, by) => {
    prevent(e);
    move(by);
  };

  const left = prepareImage(images, index - 1);
  const current = prepareImage(images, index);
  const right = prepareImage(images, index + 1);

  return (
    <div className="lightbox" onClick={handleBackdropClick}>
      <a href="#" className="lightbox__close" onClick={handleClose}></a>
      <LightboxImage
        position="left"
        data={left}
        onClick={(e) => preventAndMove(e, -1)}
      />
      <LightboxImage position="current" data={current} onClick={handleClose} />
      <LightboxImage
        position="right"
        data={right}
        onClick={(e) => preventAndMove(e, 1)}
      />
    </div>
  );
}

function LightboxImage({ data, position, onClick }) {
  return (
    <a
      className={'lightbox__image lightbox__image-' + position}
      href={data && data.url}
      style={{ height: data && data.height }}
      onClick={onClick}
    >
      {data && (
        <img
          className="lightbox__img"
          src={data && data.url}
          style={{ width: data.width, height: data.height }}
        />
      )}
    </a>
  );
}

function prepareImage(images, index) {
  const def = images[index];
  if (!def) return null;

  const availableSpace = {
    width: Math.floor(document.documentElement.clientWidth * 0.84),
    height: document.documentElement.clientHeight - 80,
  };

  const finalSize = fit(def.size, availableSpace);

  return { ...def, ...finalSize };
}

function fit(size, within) {
  size = { width: size[0], height: size[1] };
  const widthScale = within.width / size.width;
  const heightScale = within.height / size.height;
  let scale = widthScale < heightScale ? widthScale : heightScale;
  if (scale > 1) scale = 1;
  return {
    width: Math.round(size.width * scale),
    height: Math.round(size.height * scale),
  };
}
