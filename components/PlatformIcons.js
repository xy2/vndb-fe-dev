const platformInfo = {
  web: 'Web',
  lin: 'Linux',
  mac: 'macOS',
  win: 'Windows',
  ios: 'Apple iProduct',
  and: 'Android',
  dvd: 'DVD player',
  bdp: 'Blu-ray Player',
  fmt: 'FM Towns',
  gba: 'Game Boy Advance',
  gbc: 'Game Boy Color',
  msx: 'MSX',
  nds: 'Nintendo DS',
  nes: 'Famicom',
  p88: 'PC-88',
  p98: 'PC-98',
  pce: 'PC Engine',
  pcf: 'PC-FX',
  psp: 'PlayStation Portable',
  ps1: 'PlayStation 1',
  ps2: 'PlayStation 2',
  ps3: 'PlayStation 3',
  ps4: 'PlayStation 4',
  psv: 'PlayStation Vita',
  drc: 'Dreamcast',
  sat: 'Sega Saturn',
  sfc: 'Super Nintendo',
  swi: 'Nintendo Switch',
  wii: 'Nintendo Wii',
  wiu: 'Nintendo Wii U',
  n3d: 'Nintendo 3DS',
  x68: 'X68000',
  xb1: 'Xbox',
  xb3: 'Xbox 360',
  xbo: 'Xbox One',
  oth: 'Other',
};

export default function PlatformIcons({ platforms }) {
  return (
    <>
      {platforms.map((plat, i) => {
        const info = platformInfo[plat];
        if (info) {
          return (
            <img
              key={i}
              className="svg-icon"
              src={`/content/icons/platform/${plat}.svg`}
              title={info}
            />
          );
        }
      })}
    </>
  );
}
