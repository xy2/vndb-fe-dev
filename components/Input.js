import { useContext } from 'react';
import { classes } from '../lib/util';
import InputContext from '../lib/input-context';

export default function Input({ value, invalid, onChange, id }) {
  const context = useContext(InputContext);
  return (
    <input
      className={classes('form-control', { 'is-invalid': invalid })}
      id={id || (context && context.controlId)}
      value={value}
      onChange={onChange}
    />
  );
}
