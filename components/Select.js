import { classes } from '../lib/util';

export default function Select({ options, invalid }) {
  return (
    <select className={classes('form-control', { 'is-invalid': invalid })}>
      {options.map(renderOption)}
    </select>
  );
}

function renderOption(option) {
  const isObj = typeof option === 'object';
  const label = isObj ? option.label || option.value : option;
  const value = isObj ? option.value : option;
  return (
    <option key={value} value={value}>
      {label}
    </option>
  );
}
