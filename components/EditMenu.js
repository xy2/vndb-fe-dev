import Dropdown from '../components/Dropdown';
import Button from '../components/Button';

export default function EditMenu({ menu, anchor }) {
  return (
    <Dropdown menu={menu} anchor={anchor}>
      <Button caret>
        <img className="svg-icon opacity-muted" src="/content/icons/edit.svg" />
      </Button>
    </Dropdown>
  );
}
