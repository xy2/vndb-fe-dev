import { Fragment } from 'react';

export default function EntityCommaList({ data, link }) {
  return (
    <Fragment>
      {data.map((x, i) => (
        <Fragment key={x.id}>
          <a href={link(x)}>{x.name}</a>
          {i < data.length - 1 ? ', ' : ''}
        </Fragment>
      ))}
    </Fragment>
  );
}
