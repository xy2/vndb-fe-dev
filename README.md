# VNDB frontend components

Test bed for VNDB frontend components, built with [React](https://reactjs.org/) and [Next.js](https://nextjs.org/).

## Getting Started

Install dependencies:

```bash
npm install
```

Run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to view the site.
